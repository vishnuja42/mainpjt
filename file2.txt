#importing the necessary libraries and dependencies
import pandas as pd
import numpy as np
import seaborn as sns
from sklearn import preprocessing
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow import keras
from numpy import array
from keras.models import Sequential
from keras.layers import LSTM
from keras.layers import Dense
from keras import optimizers
tf.keras.optimizers.SGD
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
